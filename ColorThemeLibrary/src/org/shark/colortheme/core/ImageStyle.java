package org.shark.colortheme.core;

import android.graphics.drawable.Drawable;

public class ImageStyle {
	
	/**
	 * activity背景
	 */
	public static final int IMAGE_STYLE_ACTIVITY_BG = 1;
	
	/**
	 * actionbar背景
	 */
	public static final int IMAGE_STYLE_ACTIONBAR_BG = 2;
	
	/**
	 * dialog背景
	 */
	public static final int IMAGE_STYLE_DIALOG_BG = 3;
	
	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	/**
	 * 内存图片 
	 */
	public static final int TYPE_BITMAP = 0;
	/**
	 * 来自本地文件
	 */
	public static final int TYPE_FILE = 1;
	/**
	 * 来自APK-RES目录的图片
	 */
	public static final int TYPE_RES  = 2;
	/**
	 * 来自系统相册，但是这个图片是数据库索引
	 */
	public static final int TYPE_MEDIA_DB = 3;
	/**
	 * 来自网络文件
	 */
	public static final int TYPE_URL  = 4;
	/**
	 * 来自其他APK
	 */
	public static final int TYPE_APK = 5;
	//------------------------------------------------------------------------------
	//------------------------------------------------------------------------------
	private int mType;
	private String imageURI;
	private Drawable mDrawable;
	private  int mStyleName;
	public int getType() {
		return mType;
	}

	public void setType(int mType) {
		this.mType = mType;
	}

	public String getImageURI() {
		return imageURI;
	}

	public void setImageURI(String imageURI) {
		this.imageURI = imageURI;
	}

	public Drawable getDrawable() {
		return mDrawable;
	}

	public void setDrawable(Drawable mDrawable) {
		this.mDrawable = mDrawable;
	}

	public int getStyleName() {
		return mStyleName;
	}

	public void setStyleName(int mStyleName) {
		this.mStyleName = mStyleName;
	}

}
