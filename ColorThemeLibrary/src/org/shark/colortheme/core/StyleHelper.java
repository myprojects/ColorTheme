package org.shark.colortheme.core;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.StateListDrawable;

public class StyleHelper {

	/** 设置Selector。 */
	public static StateListDrawable newSelector(Context context, String idNormal,
			String idPressed) {
		PaintDrawable pd1 = new PaintDrawable(Color.parseColor(idNormal));
		PaintDrawable pd2 = new PaintDrawable(Color.parseColor(idPressed));
		
		pd1.setCornerRadius(5);
		pd1.setPadding(20, 20, 20, 20);
		pd2.setCornerRadius(5);
		pd2.setPadding(20, 20, 20, 20);
		StateListDrawable bg = new StateListDrawable();
		// View.PRESSED_ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_pressed,
				android.R.attr.state_enabled }, pd2);
		// View.ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_enabled }, pd1);
		// View.EMPTY_STATE_SET
		bg.addState(new int[] {}, pd1);

		return bg;
	}
	/** 设置Selector。 */
	public static StateListDrawable newSelectorList(Context context, String idNormal,
			String idPressed) {
		ColorDrawable pd1 = new ColorDrawable(Color.parseColor(idNormal));
		ColorDrawable pd2 = new ColorDrawable(Color.parseColor(idPressed));
		StateListDrawable bg = new StateListDrawable();
		// View.PRESSED_ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_pressed,
				android.R.attr.state_enabled }, pd2);
		// View.ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_enabled }, pd1);
		// View.EMPTY_STATE_SET
		bg.addState(new int[] {}, pd1);
		
		return bg;
	}
	public static StateListDrawable newSelector2(Context context, String idNormal,
			String idPressed) {
		PaintDrawable pd1 = new PaintDrawable(Color.parseColor(idNormal));
		PaintDrawable pd2 = new PaintDrawable(Color.parseColor(idPressed));
		pd1.getPaint().setStyle(Paint.Style.STROKE);
		pd1.getPaint().setStrokeWidth(2);
		pd1.setPadding(20, 20, 20, 20);
		pd1.setCornerRadius(4);
		//
		//
		pd2.setCornerRadius(5);
		pd2.setPadding(20, 20, 20, 20);
		StateListDrawable bg = new StateListDrawable();
		// View.PRESSED_ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_pressed,
				android.R.attr.state_enabled }, pd2);
		// View.ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_enabled }, pd1);
		// View.EMPTY_STATE_SET
		bg.addState(new int[] {}, pd1);
		
		return bg;
	}
	public static StateListDrawable newSelector3(Context context, String idNormal,
			String idPressed) {
		PaintDrawable pd1 = new PaintDrawable(Color.parseColor(idNormal));
		PaintDrawable pd2 = new PaintDrawable(Color.parseColor(idPressed));
		pd1.getPaint().setStyle(Paint.Style.STROKE);
		pd1.getPaint().setColor(Color.WHITE);
		pd1.getPaint().setStrokeWidth(4);
		pd1.setPadding(20, 20, 20, 20);
		pd1.setCornerRadius(8);
		//
		//
		pd2.setCornerRadius(5);
		pd2.setPadding(20, 20, 20, 20);
		StateListDrawable bg = new StateListDrawable();
		// View.PRESSED_ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_pressed,
				android.R.attr.state_enabled }, pd2);
		// View.ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_enabled }, pd1);
		// View.EMPTY_STATE_SET
		bg.addState(new int[] {}, pd1);
		
		return bg;
	}
	public static StateListDrawable newSelector33(Context context,int pading, String idNormal,
			String idPressed) {
		PaintDrawable pd1 = new PaintDrawable(Color.parseColor(idNormal));
		PaintDrawable pd2 = new PaintDrawable(Color.parseColor(idPressed));
		pd1.getPaint().setStyle(Paint.Style.STROKE);
		pd1.getPaint().setColor(Color.WHITE);
		pd1.getPaint().setStrokeWidth(4);
		pd1.setPadding(pading, pading, pading, pading);
		pd1.setCornerRadius(8);
		//
		//
		pd2.setCornerRadius(5);
		pd2.setPadding(pading, pading, pading, pading);
		StateListDrawable bg = new StateListDrawable();
		// View.PRESSED_ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_pressed,
				android.R.attr.state_enabled }, pd2);
		// View.ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_enabled }, pd1);
		// View.EMPTY_STATE_SET
		bg.addState(new int[] {}, pd1);
		
		return bg;
	}
	
	public static StateListDrawable newSelector4(Context context, String idNormal,
			String idPressed) {
		ColorDrawable pd1 = new ColorDrawable(Color.parseColor(idNormal));
		ColorDrawable pd2 = new ColorDrawable(Color.parseColor(idPressed));
		
		StateListDrawable bg = new StateListDrawable();
		// View.PRESSED_ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_pressed,
				android.R.attr.state_enabled }, pd2);
		// View.ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_enabled }, pd1);
		// View.EMPTY_STATE_SET
		bg.addState(new int[] {}, pd1);

		return bg;
	}
	
	public static StateListDrawable newSelector22(Context context, String idNormal,
			String idPressed) {
		PaintDrawable pd1 = new PaintDrawable(Color.parseColor(idNormal));
		PaintDrawable pd2 = new PaintDrawable(Color.parseColor(idPressed));
		pd1.getPaint().setStyle(Paint.Style.STROKE);
		pd1.getPaint().setStrokeWidth(2);
		pd1.setPadding(20, 20, 20, 20);
		pd1.setCornerRadius(0.1f);
		//
		//
		pd2.setCornerRadius(0.1f);
		pd2.setPadding(20, 20, 20, 20);
		StateListDrawable bg = new StateListDrawable();
		// View.PRESSED_ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_pressed,
				android.R.attr.state_enabled }, pd2);
		// View.ENABLED_STATE_SET
		bg.addState(new int[] { android.R.attr.state_enabled }, pd1);
		// View.EMPTY_STATE_SET
		bg.addState(new int[] {}, pd1);
		
		return bg;
	}
	
//	/** 设置Selector。 */
//	public static StateListDrawable newSelector(Context context, int idNormal,
//			int idPressed, int idFocused, int idUnable) {
//		StateListDrawable bg = new StateListDrawable();
//		Drawable normal = idNormal == -1 ? null : context.getResources()
//				.getDrawable(idNormal);
//		Drawable pressed = idPressed == -1 ? null : context.getResources()
//				.getDrawable(idPressed);
//		Drawable focused = idFocused == -1 ? null : context.getResources()
//				.getDrawable(idFocused);
//		Drawable unable = idUnable == -1 ? null : context.getResources()
//				.getDrawable(idUnable);
//		// View.PRESSED_ENABLED_STATE_SET
//		bg.addState(new int[] { android.R.attr.state_pressed,
//				android.R.attr.state_enabled }, pressed);
//		// View.ENABLED_FOCUSED_STATE_SET
//		bg.addState(new int[] { android.R.attr.state_enabled,
//				android.R.attr.state_focused }, focused);
//		// View.ENABLED_STATE_SET
//		bg.addState(new int[] { android.R.attr.state_enabled }, normal);
//		// View.FOCUSED_STATE_SET
//		bg.addState(new int[] { android.R.attr.state_focused }, focused);
//		// View.WINDOW_FOCUSED_STATE_SET
//		bg.addState(new int[] { android.R.attr.state_window_focused }, unable);
//		// View.EMPTY_STATE_SET
//		bg.addState(new int[] {}, normal);
//		return bg;
//	}
}
