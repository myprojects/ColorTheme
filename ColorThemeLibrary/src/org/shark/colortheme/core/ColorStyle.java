/**
 * 
 */
package org.shark.colortheme.core;

/**
 * @author hljdrl@gmail.com
 * 
 */
public class ColorStyle {

	private String color;
	private String color_pressed;
	private String color_normal;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getColor_pressed() {
		return color_pressed;
	}

	public void setColor_pressed(String color_pressed) {
		this.color_pressed = color_pressed;
	}

	public String getColor_normal() {
		return color_normal;
	}

	public void setColor_normal(String color_normal) {
		this.color_normal = color_normal;
	}

}
