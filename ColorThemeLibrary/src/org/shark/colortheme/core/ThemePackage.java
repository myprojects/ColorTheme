package org.shark.colortheme.core;

import org.shark.colortheme.util.ContextUtil;

import android.content.Context;
import android.graphics.drawable.Drawable;

/**
 * 主题包-配置，APK的主题包配置，必须初始化themeNameResId和activityBackgroundResId的数值，这个数值是R文件夹中的ID,多个APK主题包和
 * 主项目中的每个选项的ID必须一样，否则在主项目按照主题包，在访问主题包资源的时候会有资源读取不到。
 * @author hljdrl@gmail.com
 *
 */
public class ThemePackage {
	private boolean load;
	static private  int themeNameResId = 0;
	static private  int activityBackgroundResId = 0;
	/**
	 * 主题图片string在R文件中的id
	 * @param nameResId
	 */
	public static void setThemeNameResId(int nameResId){
		themeNameResId = nameResId;
	}
	/**
	 * 背景主图片在R文件中的id
	 * @param atybgResId
	 */
	public static void setActivityBackgroundResId(int atybgResId){
		activityBackgroundResId = atybgResId;
	}
	private String themeName;
	public String pkgName;
	public Drawable activityBackground;
	public String getPkgName() {
		return pkgName;
	}
	public void setPkgName(String pkgName) {
		this.pkgName = pkgName;
	}
	
	public Drawable getActivityBackground() {
		return activityBackground;
	}
	public void setActivityBackground(Drawable activityBackground) {
		this.activityBackground = activityBackground;
	}
	public int getActivityBackgroundResId() {
		return activityBackgroundResId;
	}
	public String getThemeName() {
		return themeName;
	}
	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}
	public int getThemeNameResId() {
		return themeNameResId;
	}
	public final void loading(Context ctx)
	{
		if(!isLoad()){
			Context ThemeCtx = ContextUtil.finalAppContext(ctx, getPkgName());
			if(ThemeCtx!=null){
				String _name = ContextUtil.getApkString(ThemeCtx, themeNameResId);
				setThemeName(_name);
				Drawable _drawable = ContextUtil.getDrawable(ThemeCtx, activityBackgroundResId);
				setActivityBackground(_drawable);
			}
			load = true;
		}
	}
	public boolean isLoad() {
		return load;
	}
	

}
