package org.shark.colortheme.core;

public interface ImageStyleObserver extends StyleObserver {
	
	public void onChageImageStyle(ColorStyleObservable o,ImageStyle chage);

}
