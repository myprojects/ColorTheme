package org.shark.colortheme.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Picture;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

/**
 * 图片加载器
 * 
 * @author hljdrl@gmail.com
 * 
 */
public final class ImageLoader extends AbsImageLoader {

	private static String TAG = "ImageLoader";

	/**
	 * 加载媒体库图片
	 * 
	 * @param ctx
	 * @param _uri
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public final Drawable loadKitKatMediaAsDrawable(Context ctx, Uri _uri) {
		Toast.makeText(ctx, _uri.toString(), Toast.LENGTH_LONG).show();
		Drawable mDrawable = null;
		String imgPath = getPath(ctx, _uri);
		if (imgPath != null) {
			Bitmap bm = BitmapFactory.decodeFile(imgPath);
			mDrawable = new BitmapDrawable(bm);
		} else {

		}

		try {
			if (mDrawable == null) {
				ContentResolver cr = ctx.getContentResolver();
				Bitmap bitmap = BitmapFactory.decodeStream(cr
						.openInputStream(_uri));
				if (bitmap != null) {
					mDrawable = new BitmapDrawable(bitmap);
				}
			} else {
				ColorThemeManager.getInstance()
						.setActivityBackground(mDrawable);
			}
		} catch (FileNotFoundException e) {
			Log.e("Exception", e.getMessage(), e);
		} catch (Exception ex) {
			Log.e("Exception", ex.getMessage(), ex);
		}
		return mDrawable;
	}

	@SuppressLint("NewApi")
	public static String getPath(final Context context, final Uri uri) {

		final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

		// DocumentProvider
		if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
			// ExternalStorageProvider
			if (isExternalStorageDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				if ("primary".equalsIgnoreCase(type)) {
					return Environment.getExternalStorageDirectory() + "/"
							+ split[1];
				}

			}
			// DownloadsProvider
			else if (isDownloadsDocument(uri)) {

				final String id = DocumentsContract.getDocumentId(uri);
				final Uri contentUri = ContentUris.withAppendedId(
						Uri.parse("content://downloads/public_downloads"),
						Long.valueOf(id));

				return getDataColumn(context, contentUri, null, null);
			}
			// MediaProvider
			else if (isMediaDocument(uri)) {
				final String docId = DocumentsContract.getDocumentId(uri);
				final String[] split = docId.split(":");
				final String type = split[0];

				Uri contentUri = null;
				if ("image".equals(type)) {
					contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
				} else if ("video".equals(type)) {
					contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
				} else if ("audio".equals(type)) {
					contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
				}

				final String selection = "_id=?";
				final String[] selectionArgs = new String[] { split[1] };

				return getDataColumn(context, contentUri, selection,
						selectionArgs);
			}
		} else if ("content".equalsIgnoreCase(uri.getScheme())) {

			// Return the remote address
			if (isGooglePhotosUri(uri))
				return uri.getLastPathSegment();

			return getDataColumn(context, uri, null, null);
		}
		// File
		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		} else if (!DocumentsContract.isDocumentUri(context, uri)) {
			//
			if (uri != null) {
				String uriStr = uri.toString();
				String path = uriStr.substring(10, uriStr.length());
				if (path.startsWith("com.sec.android.gallery3d")) {
					Log.e(TAG, "It's auto backup pic path:" + uri.toString());
					return null;
				}
			}
			String[] filePathColumn = { MediaStore.Images.Media.DATA };
			Cursor cursor = context.getContentResolver().query(uri,
					filePathColumn, null, null, null);
			cursor.moveToFirst();
			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String picturePath = cursor.getString(columnIndex);
			cursor.close();
			return picturePath;
		}
		return null;
	}

	/**
	 * Get the value of the data column for this Uri. This is useful for
	 * MediaStore Uris, and other file-based ContentProviders.
	 * 
	 * @param context
	 *            The context.
	 * @param uri
	 *            The Uri to query.
	 * @param selection
	 *            (Optional) Filter used in the query.
	 * @param selectionArgs
	 *            (Optional) Selection arguments used in the query.
	 * @return The value of the _data column, which is typically a file path.
	 */
	public static String getDataColumn(Context context, Uri uri,
			String selection, String[] selectionArgs) {

		Cursor cursor = null;
		final String column = "_data";
		final String[] projection = { column };

		try {
			cursor = context.getContentResolver().query(uri, projection,
					selection, selectionArgs, null);
			if (cursor != null && cursor.moveToFirst()) {
				final int index = cursor.getColumnIndexOrThrow(column);
				return cursor.getString(index);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return null;
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is ExternalStorageProvider.
	 */
	public static boolean isExternalStorageDocument(Uri uri) {
		return "com.android.externalstorage.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is DownloadsProvider.
	 */
	public static boolean isDownloadsDocument(Uri uri) {
		return "com.android.providers.downloads.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is MediaProvider.
	 */
	public static boolean isMediaDocument(Uri uri) {
		return "com.android.providers.media.documents".equals(uri
				.getAuthority());
	}

	/**
	 * @param uri
	 *            The Uri to check.
	 * @return Whether the Uri authority is Google Photos.
	 */
	public static boolean isGooglePhotosUri(Uri uri) {
		return "com.google.android.apps.photos.content".equals(uri
				.getAuthority());
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	/**
	 * 加载res图片资源
	 * 
	 * @param ctx
	 * @param resid
	 * @return
	 */
	@Override
	public final Drawable loadResourcesDrawable(Context ctx, int resid) {
		Drawable mDrawable = null;

		Resources mRes = ctx.getResources();
		try {
			mDrawable = mRes.getDrawable(resid);
		} catch (NotFoundException not) {
			not.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return mDrawable;
	}

	/**
	 * 加载本地存储图片
	 * 
	 * @param _image
	 */
	@SuppressWarnings("deprecation")
	@Override
	public final void loadPictureDrawable(ImageStyle _image) {
		if (_image != null) {
			if (_image.getType() == ImageStyle.TYPE_FILE) {
				if (_image.getImageURI() != null) {
					PictureDrawable mPictureDrawable = null;
					try {
						File file = new File(_image.getImageURI());
						if (file.exists()) {
							mPictureDrawable = new PictureDrawable(
									Picture.createFromStream(new FileInputStream(
											file)));
							_image.setDrawable(mPictureDrawable);
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * 加载图片
	 * 
	 * @param _image
	 */
	@SuppressWarnings("deprecation")
	@Override
	public void loadBitmap(ImageStyle _image) {
		if (_image != null) {
			if (_image.getType() == ImageStyle.TYPE_FILE) {
				if (_image.getImageURI() != null) {
					Drawable mDrawable = new BitmapDrawable(
							_image.getImageURI());
					_image.setDrawable(mDrawable);

				}
			}
		}
	}

	/**
	 * 加载图片，可配置加载模式
	 * 
	 * @param _image
	 * @param opt
	 */
	@SuppressWarnings("deprecation")
	@Override
	public final void loadBitmap(ImageStyle _image,
			android.graphics.BitmapFactory.Options opt) {
		if (_image != null) {
			if (_image.getType() == ImageStyle.TYPE_FILE) {
				if (_image.getImageURI() != null) {
					Bitmap _bm = BitmapFactory.decodeFile(_image.getImageURI(),
							opt);
					Drawable mDrawable = new BitmapDrawable(_bm);
					_image.setDrawable(mDrawable);

				}
			}
		}
	}

}
