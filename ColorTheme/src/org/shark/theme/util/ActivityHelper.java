package org.shark.theme.util;

import org.shark.colortheme.R;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public final class ActivityHelper {

	protected Activity mActivity;

	final public static ActivityHelper createInstance(Activity activity) {
		return new ActivityHelper(activity);

	}

	protected ActivityHelper(Activity activity) {
		mActivity = activity;
	}

	final public void setupActionBar(CharSequence title, int color) {
		final ViewGroup actionBarCompat = getActionBarCompat();
		if (actionBarCompat == null) {
			return;
		}

	}

	public final void setupActionLeftButton(String title,
			StateListDrawable drawable, OnClickListener l) {
		final ViewGroup actionBarCompat = getActionBarCompat();
		if (actionBarCompat == null) {
			return;
		}
		Button btn = (Button) actionBarCompat
				.findViewById(R.id.actionbar_left_btn);
		if (btn != null) {
			btn.setOnClickListener(l);
			if (drawable != null) {
				btn.setBackgroundDrawable(drawable);
			}
			if (title == null) {
				btn.setVisibility(View.GONE);
			} else {
				btn.setVisibility(View.VISIBLE);
				btn.setText(title);

			}
		}
	}

	public final void setupActionLeftButton(int title, OnClickListener l) {
		final ViewGroup actionBarCompat = getActionBarCompat();
		if (actionBarCompat == null) {
			return;
		}
		Button btn = (Button) actionBarCompat
				.findViewById(R.id.actionbar_left_btn);
		if (btn != null) {
			btn.setOnClickListener(l);
			if (title < 0) {
				btn.setVisibility(View.GONE);
			} else {
				// btn.setText(title);
				btn.setVisibility(View.VISIBLE);
			}
		}
	}

	public final void setupActionRightButton(String title, OnClickListener l) {
		final ViewGroup actionBarCompat = getActionBarCompat();
		if (actionBarCompat == null) {
			return;
		}

		Button btn = (Button) actionBarCompat
				.findViewById(R.id.actionbar_right_btn);
		if (btn != null) {
			btn.setOnClickListener(l);
			if (title == null) {
				btn.setVisibility(View.GONE);
			} else {
				btn.setText(title);
				btn.setVisibility(View.VISIBLE);
			}
		}

	}

	public final void uninstallRightButton() {
		final ViewGroup actionBarCompat = getActionBarCompat();
		if (actionBarCompat == null) {
			return;
		}
		Button btn = (Button) actionBarCompat
				.findViewById(R.id.actionbar_right_btn);
		btn.setOnClickListener(null);
		btn.setVisibility(View.GONE);
	}

	public final void setupActionRightButton(int title, OnClickListener l) {
		final ViewGroup actionBarCompat = getActionBarCompat();
		if (actionBarCompat == null) {
			return;
		}

		Button btn = (Button) actionBarCompat
				.findViewById(R.id.actionbar_right_btn);
		if (btn != null) {
			btn.setOnClickListener(l);
			if (title < 0) {
				btn.setVisibility(View.GONE);
			} else {
				btn.setText(title);
				btn.setVisibility(View.VISIBLE);
			}
		}

	}

	final public void setActionBarTitle(CharSequence title) {
		ViewGroup actionBar = getActionBarCompat();
		if (actionBar == null) {
			return;
		}

		TextView titleText = (TextView) actionBar
				.findViewById(R.id.actionbar_compat_text);
		if (titleText != null) {
			titleText.setText(title);
		}
	}

	final public void setActionBarTitle(int title) {
		ViewGroup actionBar = getActionBarCompat();
		if (actionBar == null) {
			return;
		}

		TextView titleText = (TextView) actionBar
				.findViewById(R.id.actionbar_compat_text);
		if (titleText != null) {
			titleText.setText(title);
		}
	}

	final public void setActionBarTitle(String title, OnClickListener l) {
		ViewGroup actionBar = getActionBarCompat();
		if (actionBar == null) {
			return;
		}

		TextView titleText = (TextView) actionBar
				.findViewById(R.id.actionbar_compat_text);
		if (titleText != null) {
			titleText.setText(title);
			titleText.setOnClickListener(l);
		}
	}

	final public void setActionBarBackrround(Drawable drawable) {
		ViewGroup actionBar = getActionBarCompat();
		if (actionBar == null) {
			return;
		}
		actionBar.setBackgroundDrawable(drawable);
	}

	final public void setActionBarBackground(int color) {
		ViewGroup actionBar = getActionBarCompat();
		if (actionBar == null) {
			return;
		}
		actionBar.setBackgroundColor(color);
	}

	final public void setActionBarVisible(int visible) {
		getActionBarCompat().setVisibility(visible);
	}

	final public ViewGroup getActionBarCompat() {
		return (ViewGroup) mActivity.findViewById(R.id.actionbar_compat);
	}

}
