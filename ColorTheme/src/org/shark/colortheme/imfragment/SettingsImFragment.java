package org.shark.colortheme.imfragment;

import org.shark.colortheme.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SettingsImFragment extends Fragment {
	private View rootView;

	public static SettingsImFragment newInstance() {
		SettingsImFragment fragment = new SettingsImFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.root_ft_imsettings, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
	}
}
