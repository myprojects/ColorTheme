package org.shark.colortheme;

import org.shark.colortheme.adapter.ImChatPageAdapter;
import org.shark.colortheme.core.ColorStyle;
import org.shark.colortheme.core.ColorStyleObservable;
import org.shark.colortheme.core.ColorThemeManager;
import org.shark.colortheme.core.ImageStyle;
import org.shark.colortheme.core.ImageStyleObserver;
import org.shark.colortheme.core.StyleObserver;
import org.shark.colortheme.view.PagerImageTabStrip;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

/**
 * 主页面
 * 
 * @author hljdrl@gmail.com
 * 
 */
public class ImChatActivity extends BaseActivity implements ImageStyleObserver{
	public static final String[] TITLES = { "会话", "通讯录", "设置" };
	private ImChatPageAdapter pageAdapter;
	private PagerImageTabStrip tabs;
	private ViewPager mPager;
	private ColorStyle colorStyle;
	private String TAG = "ImChatActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getWindow().setBackgroundDrawableResource(
				R.drawable.activity_background);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.root_im_chat);
		colorStyle = ColorThemeApplication.getInstance().getColorStyle();
		setupRootLayout();
		ColorThemeManager.getInstance().setupActionBar(getActivityHelper().getActionBarCompat());
	
		//设置activity的背景
		ColorThemeManager.getInstance().setupActivityBackground(this);
		//注册activity背景改变通知接口
		ColorThemeManager.getInstance().addStyleObserver(this);
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		//注销activity背景改变通知接口
		ColorThemeManager.getInstance().deleteStyleObserver(this);
	}
	//动态改变接口
	@Override
	public void onChageImageStyle(ColorStyleObservable o, ImageStyle chage) {
			if(chage!=null && chage.getDrawable()!=null){
				getWindow().setBackgroundDrawable(chage.getDrawable());
			}
	}
	

	private void setupRootLayout() {
		setupAction();
		setupView();
	}

	private void setupView() {
		pageAdapter = new ImChatPageAdapter(getSupportFragmentManager(), TITLES);
		tabs = (PagerImageTabStrip) findViewById(R.id.home_tabStrip);
		mPager = (ViewPager) findViewById(R.id.home_viewpager);
		mPager.setAdapter(pageAdapter);
		tabs.setViewPager(mPager);
		tabs.setTabBackground(colorStyle);
	}

	private void setupAction() {
		getActivityHelper().setActionBarTitle(R.string.app_name);
	}






}
