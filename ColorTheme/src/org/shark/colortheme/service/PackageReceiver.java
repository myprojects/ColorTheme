package org.shark.colortheme.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

public class PackageReceiver extends BroadcastReceiver {

	@SuppressWarnings("unused")
	@Override
	public void onReceive(Context context, Intent intent) {
		PackageManager manager = context.getPackageManager();
		if (intent.getAction().equals(Intent.ACTION_PACKAGE_ADDED)) {
			String packageName = intent.getData().getSchemeSpecificPart();
			Intent mIm = new Intent(context, PackageService.class);
			mIm.putExtra("pkgName", packageName);
			mIm.putExtra("type", "add");
			context.startService(mIm);
		}
		if (intent.getAction().equals(Intent.ACTION_PACKAGE_REMOVED)) {
			String packageName = intent.getData().getSchemeSpecificPart();
			Intent mIm = new Intent(context, PackageService.class);
			mIm.putExtra("pkgName", packageName);
			mIm.putExtra("type", "del");
			context.startService(mIm);
			
		}
		if (intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED)) {
			String packageName = intent.getData().getSchemeSpecificPart();
			
		}
	}

}
