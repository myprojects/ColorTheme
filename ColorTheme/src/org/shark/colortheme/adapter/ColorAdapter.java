package org.shark.colortheme.adapter;

import java.util.List;

import org.shark.colortheme.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class ColorAdapter extends ArrayAdapter<ColorItems> {

	private LayoutInflater mInflater;

	public ColorAdapter(Context context, List<ColorItems> list) {
		super(context, 0, list);
		mInflater = LayoutInflater.from(context);
		
	}
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		final ColorItems info = getItem(position);
		if (view == null) {
			view = mInflater.inflate(R.layout.grid_color_item, parent, false);
		}
		if (info.color != -1) {
			ImageView icon = (ImageView) view.findViewById(R.id.grid_image);
			icon.setBackgroundColor(info.color);
		}
		return view;
	}
}
