package org.shark.colortheme.adapter;

import java.util.HashMap;
import java.util.Map;
import org.shark.colortheme.fragment.AboutFragment;
import org.shark.colortheme.fragment.ColorFragment;
import org.shark.colortheme.fragment.ImageFragment;
import org.shark.colortheme.fragment.SettingFragment;
import org.shark.colortheme.fragment.ThemeLoaderFragment;
import org.shark.colortheme.fragment.ViewFragment;
import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

public class TitlePageAdapter extends FragmentStatePagerAdapter {

	private String[] TITLES;
	 @SuppressLint("UseSparseArrays")
	Map<Integer, Fragment> mCacheFragments = new HashMap<Integer, Fragment>();
	
	public TitlePageAdapter(FragmentManager fm) {
		super(fm);
	}

	public TitlePageAdapter(FragmentManager fm, String[] titles) {
		super(fm);
		TITLES = titles;
	}

	public Fragment getItem(int arg0) {
		Fragment mFm = null;
		if (mCacheFragments.containsKey(arg0)) {
			mFm = mCacheFragments.get(arg0);
		} else {
			switch (arg0) {
			case 0:
				mFm = ImageFragment.newInstance();
				break;
			case 1:
				mFm  = ColorFragment.newInstance();
				break;
			case 2:
				mFm =ThemeLoaderFragment.newInstance();
				break;
			case 3:
				mFm =  ViewFragment.newInstance();
				break;
			case 4:
				mFm =  SettingFragment.newInstance();
				break;
			case 5:
				mFm =  AboutFragment.newInstance();
				break;
			default:
				break;
			}
			mCacheFragments.put(arg0, mFm);
		}
		return mFm;
	}

	@Override
	public int getCount() {
		return TITLES.length;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return TITLES[position % TITLES.length];
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		super.destroyItem(container, position, object);
	}
}
