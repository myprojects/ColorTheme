package org.shark.colortheme.adapter;

import java.util.List;
import org.shark.colortheme.R;
import org.shark.colortheme.core.ThemePackage;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ThemeAdapter extends ArrayAdapter<ThemePackage> {
	LayoutInflater in = null;
	public ThemeAdapter(Context context,List<ThemePackage> objects) {
		super(context, 0, objects);
		in = LayoutInflater.from(context);
	}
	
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View mVm, ViewGroup parent) {
		final ThemePackage mTm = getItem(position);
		if(mVm==null){
			mVm = in.inflate(R.layout.item_theme_grid, null);
		}
		final ImageView image = (ImageView) mVm.findViewById(R.id.item_image_theme);
		final TextView text = (TextView) mVm.findViewById(R.id.item_text_theme);
		if(mTm.isLoad()){
		text.setText(mTm.getThemeName());
		image.setImageDrawable(mTm.getActivityBackground());
		}else{
			mVm.post(new Runnable() {
				
				@Override
				public void run() {
					mTm.loading(getContext());
					text.setText(mTm.getThemeName());
					image.setImageDrawable(mTm.getActivityBackground());
				}
			});
		}
		//
		return mVm;
	}

}
