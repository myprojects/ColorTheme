package org.shark.colortheme.fragment;

import java.util.ArrayList;
import java.util.List;

import org.shark.colortheme.ColorThemeApplication;
import org.shark.colortheme.HomeActivity;
import org.shark.colortheme.R;
import org.shark.colortheme.adapter.ColorAdapter;
import org.shark.colortheme.adapter.ColorItems;
import org.shark.colortheme.core.ColorHelper;
import org.shark.colortheme.core.ColorStyle;
import org.shark.colortheme.util.SPHelper;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

/**
 * 
 * @author wang.song
 * 
 */
public class ColorPanel extends DialogFragment implements OnItemClickListener , OnClickListener{
	public static final String Tag = ColorPanel.class.getName();
	private static ColorPanel install;
	private GridView gridView;
	private View rootView;
	private ColorAdapter adapter;
	private TextView tv_title;
	private View view_cancel;
	private ColorStyle colorStyle;

	public static ColorPanel newInstance() {
		if (install == null) {
			install = new ColorPanel();
		}
		return install;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(DialogFragment.STYLE_NO_FRAME, R.style.Theme_Option);
		colorStyle = ColorThemeApplication.getInstance().getColorStyle();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.option_color_panel, container, false);
	}

	@Override
	public void onActivityCreated(Bundle arg0) {
		super.onActivityCreated(arg0);
		rootView = getView();
		tv_title = (TextView) rootView.findViewById(R.id.tv_color_title);
		tv_title.setBackgroundColor(Color.parseColor(colorStyle.getColor()));
		view_cancel = rootView.findViewById(R.id.view_cancel);
		gridView = (GridView) rootView.findViewById(R.id.gridView_color);
		adapter = new ColorAdapter(getActivity(), getTestList());
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(this);
		view_cancel.setOnClickListener(this);
	}

	private List<ColorItems> getTestList() {
		List<ColorItems> mLm = new ArrayList<ColorItems>();
		ColorItems item1 = new ColorItems();
		item1.setColor(Color.parseColor("#1ABC9C"));
		ColorItems item2 = new ColorItems();
		item2.setColor(Color.parseColor("#F1C40F"));
		ColorItems item3 = new ColorItems();
		item3.setColor(Color.parseColor("#2ECC71"));
		ColorItems item4 = new ColorItems();
		item4.setColor(Color.parseColor("#E67E22"));
		ColorItems item5 = new ColorItems();
		item5.setColor(Color.parseColor("#3498DB"));
		ColorItems item6 = new ColorItems();
		item6.setColor(Color.parseColor("#E74C3C"));
		ColorItems item7 = new ColorItems();
		item7.setColor(Color.parseColor("#9B59B6"));
		ColorItems item8 = new ColorItems();
		item8.setColor(Color.parseColor("#ECF0F1"));
		ColorItems item9 = new ColorItems();
		item9.setColor(Color.parseColor("#34495E"));
		ColorItems item10 = new ColorItems();
		item10.setColor(Color.parseColor("#95A5A6"));
		ColorItems item11 = new ColorItems();
		item11.setColor(Color.parseColor("#ff68ab"));
		ColorItems item12 = new ColorItems();
		item12.setColor(Color.parseColor("#072e79"));
		
		mLm.add(item1);
		mLm.add(item2);
		mLm.add(item3);
		mLm.add(item4);
		mLm.add(item5);
		mLm.add(item6);
		mLm.add(item7);
		mLm.add(item8);
		mLm.add(item9);
		mLm.add(item10);
		mLm.add(item11);
		mLm.add(item12);
		return mLm;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ColorItems  item = adapter.getItem(position);
		String strColor = ColorHelper.convertToARGB(item.getColor());
		SPHelper.setColor(getActivity(), strColor);
		ColorStyle style = new ColorStyle();
		style.setColor(strColor);
		style.setColor_normal(strColor);
		style.setColor_pressed(ColorHelper.convertToPressed(strColor));
		ColorThemeApplication.getInstance().setColorStyle(style);
		HomeActivity act = (HomeActivity) getActivity();
		act.changeColor(item.getColor());
		dismiss();
	}

	@Override
	public void onClick(View v) {
		dismiss();
	}

}