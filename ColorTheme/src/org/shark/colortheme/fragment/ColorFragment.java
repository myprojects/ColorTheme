package org.shark.colortheme.fragment;

import java.util.ArrayList;
import java.util.List;

import org.shark.colortheme.ColorThemeApplication;
import org.shark.colortheme.HomeActivity;
import org.shark.colortheme.R;
import org.shark.colortheme.adapter.ColorAdapter;
import org.shark.colortheme.adapter.ColorItems;
import org.shark.colortheme.core.ColorHelper;
import org.shark.colortheme.core.ColorStyle;
import org.shark.colortheme.core.ColorStyleObservable;
import org.shark.colortheme.core.ColorStyleObserver;
import org.shark.colortheme.core.StyleHelper;
import org.shark.colortheme.util.SPHelper;

import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ColorFragment extends Fragment implements OnItemClickListener,
		ColorStyleObserver {
	private View rootView;
	private ListView listView;
	private ColorAdapter adapter;
	private ColorStyle colorStyle;


	public static ColorFragment newInstance() {
		ColorFragment fragment = new ColorFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		colorStyle = ColorThemeApplication.getInstance().getColorStyle();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.root_ft_color, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		adapter = new ColorAdapter(getActivity(), getTestList());
		listView = (ListView) rootView.findViewById(R.id.listView_color);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		StateListDrawable listStyle = StyleHelper.newSelectorList(
				getActivity(), "#00000000", colorStyle.getColor_pressed());
		listView.setSelector(listStyle);
		ColorThemeApplication.getInstance().addStyleObserver(this);

	}


	private List<ColorItems> getTestList() {
		List<ColorItems> mLm = new ArrayList<ColorItems>();
		ColorItems item1 = new ColorItems();
		item1.setColor(Color.parseColor("#1ABC9C"));
		ColorItems item2 = new ColorItems();
		item2.setColor(Color.parseColor("#F1C40F"));
		ColorItems item3 = new ColorItems();
		item3.setColor(Color.parseColor("#2ECC71"));
		ColorItems item4 = new ColorItems();
		item4.setColor(Color.parseColor("#E67E22"));
		ColorItems item5 = new ColorItems();
		item5.setColor(Color.parseColor("#3498DB"));

		mLm.add(item1);
		mLm.add(item2);
		mLm.add(item3);
		mLm.add(item4);
		mLm.add(item5);
		return mLm;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		ColorItems item = adapter.getItem(position);
		String strColor = ColorHelper.convertToARGB(item.getColor());
		SPHelper.setColor(getActivity(), strColor);
		ColorStyle style = new ColorStyle();
		style.setColor(strColor);
		style.setColor_normal(strColor);
		style.setColor_pressed(ColorHelper.convertToPressed(strColor));
		ColorThemeApplication.getInstance().setColorStyle(style);
		HomeActivity act = (HomeActivity) getActivity();
		act.changeColor(item.getColor());
	}

	@Override
	public void onChageColorStyle(ColorStyleObservable o, ColorStyle chage) {
		StateListDrawable listStyle = StyleHelper.newSelectorList(
				getActivity(), "#00000000", chage.getColor_pressed());
		listView.setSelector(listStyle);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		ColorThemeApplication.getInstance().deleteStyleObserver(this);
	}
}
