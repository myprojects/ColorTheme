package org.shark.colortheme.fragment;

import org.shark.colortheme.ColorThemeApplication;
import org.shark.colortheme.HomeActivity;
import org.shark.colortheme.R;
import org.shark.colortheme.core.ColorStyle;
import org.shark.colortheme.core.ColorStyleObservable;
import org.shark.colortheme.core.ColorStyleObserver;
import org.shark.colortheme.core.StyleHelper;

import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class ViewFragment extends Fragment implements ColorStyleObserver {
	private View rootView;
	private TextView textView;
	private ImageButton imageButton;
	private Button button;
	private Button button2;
	private Button button3;
	private Button button4;
	private ColorStyle colorStyle;

	public static ViewFragment newInstance() {
		ViewFragment fragment = new ViewFragment();
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		colorStyle = ColorThemeApplication.getInstance().getColorStyle();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.root_ft_view, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		textView = (TextView) rootView.findViewById(R.id.textView1);
		imageButton = (ImageButton) rootView.findViewById(R.id.imageButton1);
		button = (Button) rootView.findViewById(R.id.button1);
		button2 = (Button) rootView.findViewById(R.id.button2);
		button3 = (Button) rootView.findViewById(R.id.button3);
		button4 = (Button) rootView.findViewById(R.id.button4);
		textView.setTextColor(Color.parseColor(colorStyle.getColor()));

		StateListDrawable buttonStyle1 = StyleHelper.newSelector(getActivity(),
				colorStyle.getColor_normal(), colorStyle.getColor_pressed());
		StateListDrawable buttonStyle2 = StyleHelper.newSelector(getActivity(),
				colorStyle.getColor_normal(), colorStyle.getColor_pressed());
		StateListDrawable buttonStyle3 = StyleHelper.newSelector2(
				getActivity(), colorStyle.getColor_normal(),
				colorStyle.getColor_normal());
		StateListDrawable buttonStyle4 = StyleHelper.newSelector4(
				getActivity(), colorStyle.getColor_normal(),
				colorStyle.getColor_pressed());
		StateListDrawable buttonStyle5 = StyleHelper.newSelector22(
				getActivity(), colorStyle.getColor_normal(),
				colorStyle.getColor_normal());
		
		

		imageButton.setBackgroundDrawable(buttonStyle1);
		button.setBackgroundDrawable(buttonStyle2);
		button2.setBackgroundDrawable(buttonStyle3);
		button3.setBackgroundDrawable(buttonStyle4);
		button4.setBackgroundDrawable(buttonStyle5);
		ColorThemeApplication.getInstance().addStyleObserver(this);

	}

	@Override
	public void onChageColorStyle(ColorStyleObservable o, ColorStyle chage) {
		StateListDrawable buttonStyle1 = StyleHelper.newSelector(getActivity(),
				chage.getColor_normal(), chage.getColor_pressed());
		StateListDrawable buttonStyle2 = StyleHelper.newSelector(getActivity(),
				chage.getColor_normal(), chage.getColor_pressed());
		StateListDrawable buttonStyle3 = StyleHelper.newSelector2(
				getActivity(), chage.getColor_normal(),
				chage.getColor_normal());
		StateListDrawable buttonStyle4 = StyleHelper.newSelector4(
				getActivity(), chage.getColor_normal(),
				chage.getColor_pressed());
		StateListDrawable buttonStyle5 = StyleHelper.newSelector22(
				getActivity(), chage.getColor_normal(),
				chage.getColor_normal());
		 

		imageButton.setBackgroundDrawable(buttonStyle1);
		button.setBackgroundDrawable(buttonStyle2);
		button2.setBackgroundDrawable(buttonStyle3);
		button3.setBackgroundDrawable(buttonStyle4);
		button4.setBackgroundDrawable(buttonStyle5);
		textView.setTextColor(Color.parseColor(chage.getColor()));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		ColorThemeApplication.getInstance().deleteStyleObserver(this);
	}

}
