package org.shark.colortheme.fragment;

import org.shark.colortheme.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class AboutFragment extends Fragment {
	private View rootView;
	TextView aboutBody;
	TextView infoBody;

	public static AboutFragment newInstance() {
		AboutFragment fragment = new AboutFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.root_ft_about, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		rootView = getView();
		aboutBody = (TextView) rootView.findViewById(R.id.about_body);
		infoBody = (TextView) rootView.findViewById(R.id.info_body);
		
		aboutBody.setText("项目主页： http://git.oschina.net/hljdrl/ColorTheme ");
		infoBody.setText("app主题颜色设置\n根据个人喜好的颜色设置app主题颜色");
	}
}
