###email ： hljdrl@gmail.com
###ColorTheme Android 颜色主题框架
###ColorThemeClassic 经典主题-APK包
###ColorThemeStar 星空主题-APK包

###先安装ColorTheme 项目所产生的APK，然后打开在Android设备运行ColorTheme，进入到APK主题加载器页面，此时在用Eclipse部署 ColorThemeClassic
和ColorThemeStar项目，ColorTheme的APK主题页面会监听安装APK情况并识别主题APK。

##Activity设置背景图片的时候图标最好是JPG格式，作者用过JPG和PNG格式图片，在设置PNG图片为activity背景时候会出现闪屏问题。

![image](http://git.oschina.net/hljdrl/ColorTheme/raw/master/ColorTheme/screenshots/device-2014-07-03-223536.jpg)

![image](http://git.oschina.net/hljdrl/ColorTheme/raw/master/ColorTheme/screenshots/device-2014-07-03-223553.jpg)

![image](http://git.oschina.net/hljdrl/ColorTheme/raw/master/ColorTheme/screenshots/device-2014-07-04-131420.jpg)
